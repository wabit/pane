from soco.discovery import by_name
from flask import Flask, render_template
import os.path


player = "Office"


TEMPLATE_DIR = os.path.abspath('../templates')
STATIC_DIR = os.path.abspath('static')

app = Flask(__name__, static_folder=STATIC_DIR)


@app.route('/')
def playing():
    current_sonos = by_name(player)
    track = current_sonos.get_current_track_info()
    artist = track['artist']
    title = track['title']
    album = track['album']
    art = track['album_art']
    nowplaying = {
        "artist": artist,
        "title": title,
        "album": album,
        "art": art
    }
    return render_template(
        'test.html', player=player, info=nowplaying)
